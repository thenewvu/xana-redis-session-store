'use strict';

const expressSession = require('express-session');
const SessionStore   = require('connect-redis')(expressSession);
module.exports       = SessionStore;
